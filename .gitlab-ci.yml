# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: registry.gitlab.com/dmt-development/dmt:latest

#to run this locally:
#login to container registry
#>docker login registry.gitlab.com
#run image:
#>gitlab-runner exec docker run_core

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

stages:
  - format
  - test
  - deploy
  - release

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - source /venv/bin/activate
  - pip install -e .

check_format:
  stage: format
  script:
    - source /venv/bin/activate
    - black --check --diff -v .
  only:
    - main
    - pre-main

test_DMT:
  stage: test
  script:
    - source /venv/bin/activate
    - pytest --cov=DMT/ test/test_core_no_interfaces/
    - pytest --cov=DMT/ --cov-append test/test_interface_ngspice/test_*.py
    - pytest --cov=DMT/ --cov-append test/test_interface_xyce/test_*.py
    - pytest --cov=DMT/ --cov-append test/test_verilogae/test_*.py
    # Hdev:
    - git clone https://gitlab.com/metroid120/hdev_simulator.git
    - cd hdev_simulator/HdevPy #HdevPy depends on DMT_core and therefore is installed here
    - pip install -e .
    - cd ..
    # download the executable and put it into path
    - wget "https://gitlab.com/metroid120/hdev_simulator/-/jobs/artifacts/master/raw/builddir_docker/hdev?job=build:linux" -O hdev
    - chmod +x hdev
    - export PATH=$PATH:$(pwd)
    # now create the DMT simulation directory
    - mkdir -p ~/.DMT/simulation_results
    # run many test
    - python HdevPy/scripts/run_tests.py
    - cd ..
    - pytest --cov-report term-missing --cov-report html --cov=DMT/ --cov-append test/test_interface_hdev/test_*.py | tee coverage_report.txt
    - coverage=$(python3 -c "import re; match = ''; match=re.search(r'TOTAL.+?([0-9]+)%', open('coverage_report.txt').read()); print(match.group(1))")
    - anybadge -l coverage -v $coverage -f badge_coverage.svg coverage
  coverage: "/TOTAL.+?([0-9]+)%/"
  artifacts:
    paths:
      - coverage_report.txt
      - htmlcov
      - badge_coverage.svg
  only:
    - main
    - pre-main

wheel:
  stage: deploy
  script:
    - source /venv/bin/activate
    - python3 setup.py sdist bdist_wheel
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
  artifacts:
    paths:
      - dist/*.whl
  only:
    - main

pages:
  stage: deploy
  script:
    - source /venv/bin/activate
    - cd doc ; make html
    - mv build/html/ ../public/
  artifacts:
    paths:
      - public
  only:
    - main

release_to_pip:
  stage: release
  rules:
    - if: $CI_COMMIT_TAG     # Run this job when a tag is created manually
  script:
    - echo "Running the release job."
    - python3 -c "import re; match = re.search(r'(## \[\d.+?)((## \[)|\Z)', open('CHANGELOG').read(), re.DOTALL); print(match.group(1))" | tee current_changes.md
    # push to pip
    - TWINE_PASSWORD=${PIP_JOB_TOKEN} TWINE_USERNAME=__token__ python -m twine upload --repository-url https://upload.pypi.org/legacy/  dist/*
  release:
    tag_name: $CI_COMMIT_TAG
    description: './current_changes.md'
